package ru.t1.mayornikov.tm.command.project;

import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class ProjectStatusStartByIdCommand extends AbstractProjectCommand{

    private static final String NAME = "project-start-by-id";

    private static final String DESCRIPTION = "Set status project to started by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatus(id, Status.IN_PROGRESS);
    }

}