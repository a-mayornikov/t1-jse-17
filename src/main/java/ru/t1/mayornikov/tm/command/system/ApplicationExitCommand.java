package ru.t1.mayornikov.tm.command.system;

public class ApplicationExitCommand extends AbstractSystemCommand {

    private final static String NAME = "exit";

    private final static String DESCRIPTION = "Close application.";

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}