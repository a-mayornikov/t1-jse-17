package ru.t1.mayornikov.tm.command.system;

import ru.t1.mayornikov.tm.api.service.IProjectService;
import ru.t1.mayornikov.tm.api.service.ITaskService;
import ru.t1.mayornikov.tm.command.AbstractCommand;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Project;

public class DemoDataInitCommand extends AbstractSystemCommand {

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    private final static String NAME = "demo-data";

    private final static String DESCRIPTION = "Initialize demo data.";

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        getProjectService().add(new Project("SUPER PROJECT one", "", Status.IN_PROGRESS));
        getProjectService().add(new Project("DUPER PROJECT two", "Like super project one but duper.", Status.NOT_STARTED));
        getProjectService().add(new Project("donnuiy", "This project status is COMPLETED.", Status.COMPLETED));
        getTaskService().create("delo", "delat");
        getTaskService().create("BIG DEAL", "GREAT LIFE");
        getTaskService().create("order", "make model Oleg");
        System.out.println("[DEMO LOAD SUCCESS]");
    }

}