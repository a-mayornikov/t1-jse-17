package ru.t1.mayornikov.tm.command.system;

import ru.t1.mayornikov.tm.api.service.ICommandService;
import ru.t1.mayornikov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}