package ru.t1.mayornikov.tm.service;

import ru.t1.mayornikov.tm.api.repository.IProjectRepository;
import ru.t1.mayornikov.tm.api.repository.ITaskRepository;
import ru.t1.mayornikov.tm.api.service.ITaskService;
import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.exception.entity.TaskNotFoundException;
import ru.t1.mayornikov.tm.exception.field.*;
import ru.t1.mayornikov.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    @Override
    public void showTask(final Task task) {
        if (task == null) return;
        final String id = task.getId();
        final String projectId = task.getProjectId();
        final String name = task.getName();
        final String description = task.getDescription();
        final String status = Status.toName(task.getStatus());
        if (!"".equals(id)) System.out.println("ID: " + id);
        if (projectId != null) System.out.println("PROJECT ID: " + projectId);
        if (!"".equals(name)) System.out.println("NAME: " + name);
        if (!"".equals(description)) System.out.println("TO DO: " + description);
        if (!"".equals(status)) System.out.println("STATUS: " + status);
    }

    @Override
    public void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (Task task : tasks) {
            if (task == null) continue;
            System.out.println(index++ + ".");
            showTask(task);
        }
        if (index == 1) System.out.println("No one task found... ");
    }

    @Override
    public void renderTasks() {
        int index = 1;
        for(final Task task : findAll()) {
            if (task == null) continue;
            System.out.println(index++ + ".");
            showTask(task);
        }
        if (index == 1) System.out.println("No one task found... ");
    }

    public TaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        else if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return taskRepository.create(name, description);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return taskRepository.create(name);
    }

    @Override
    public Task findOne(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findOne(id);
    }

    @Override
    public Task findOne(final Integer index) {
        if (index == null || index < 0) throw new IndexEmptyException();
        return taskRepository.findOne(index);
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.remove(task);
    }

    @Override
    public Task remove(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.remove(id);
    }

    @Override
    public Task remove(final Integer index) {
        if (index == null || index < 0) throw new IndexEmptyException();
        return taskRepository.remove(index);
    }

    @Override
    public Integer getSize() {
        return taskRepository.getSize();
    }

    @Override
    public Task add(final Task task) {
        if(task == null) throw new TaskNotFoundException();
        return taskRepository.add(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        final Comparator<Task> comparator = (Comparator<Task>) sort.getComparator();
        if (comparator == null) return findAll();
        return findAll(comparator);
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        if (projectRepository.existsById(projectId)) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task update(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOne(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task update(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOne(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatus(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findOne(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatus(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (index >= taskRepository.getSize()) throw new IndexOutOfBounceException();
        final Task task = findOne(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public boolean existsById(String id) {
        return findOne(id) != null;
    }

}
