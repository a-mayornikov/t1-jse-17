package ru.t1.mayornikov.tm.service;

import ru.t1.mayornikov.tm.api.repository.IProjectRepository;
import ru.t1.mayornikov.tm.api.service.IProjectService;
import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.mayornikov.tm.exception.field.*;
import ru.t1.mayornikov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository IProjectRepository) {
        this.projectRepository = IProjectRepository;
    }

    @Override
    public void showProject(final Project project) {
        if (project == null) return;
        final String id = project.getId();
        final String name = project.getName();
        final String description = project.getDescription();
        final Status status = project.getStatus();
        if (!"".equals(id)) System.out.println("ID: " + id);
        if (!"".equals(name)) System.out.println("NAME: " + name);
        if (!"".equals(description)) System.out.println("DESCRIPTION: " + description);
        if (status != null) System.out.println("STATUS: " + Status.toName(status));
    }

    @Override
    public void renderProjects() {
        int index = 1;
        for (final Project project : findAll()) {
            if (project == null) continue;
            System.out.println(index++ + ".");
            showProject(project);
        }
        if (index == 1) System.out.println("No one project found...");
    }

    @Override
    public void renderProjects(final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index++ + ".");
            showProject(project);
        }
        if (index == 1) System.out.println("No one project found...");
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return projectRepository.create(name, description);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return projectRepository.create(name);
    }

    @Override
    public Project findOne(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findOne(id);
    }

    @Override
    public Project findOne(final Integer index) {
        if (index == null || index < 0) throw new IndexEmptyException();
        return projectRepository.findOne(index);
    }

    @Override
    public Project remove(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.remove(project);
    }

    @Override
    public Project remove(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.remove(id);
    }

    @Override
    public Project remove(final Integer index) {
        if (index == null || index < 0) throw new IndexEmptyException();
        return projectRepository.remove(index);
    }

    @Override
    public Integer getSize() {
        return projectRepository.getSize();
    }

    @Override
    public Project add(final Project project) {
        if(project == null) throw new ProjectNotFoundException();
        return projectRepository.add(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public boolean existsById(String id) {
        return findOne(id) != null;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) return findAll();
        final Comparator<Project> comparator = (Comparator<Project>) sort.getComparator();
        if (comparator == null) return findAll();
        return findAll(comparator);
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return findAll();
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project update(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOne(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project update(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOne(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatus(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOne(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatus(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (index >= projectRepository.getSize()) throw new IndexOutOfBounceException();
        final Project project = findOne(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}