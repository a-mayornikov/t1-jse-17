package ru.t1.mayornikov.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityException{

    public TaskNotFoundException() {
        super("Task not found...");
    }

}