package ru.t1.mayornikov.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFieldException{

    public TaskIdEmptyException() {
        super("Task id is empty...");
    }
    
}