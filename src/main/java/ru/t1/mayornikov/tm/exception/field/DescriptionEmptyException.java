package ru.t1.mayornikov.tm.exception.field;

public final class DescriptionEmptyException extends AbstractFieldException{

    public DescriptionEmptyException() {
        super("Description is empty...");
    }

}